
         
        const canciones = document.querySelectorAll(".audio-juego")
        let cancionActual = null
        let contenedor = document.querySelector(".contenedor");
        let tableroPuntuacion = document.getElementById("puntos");
        let puntos = 0;
        let isPaused = true;
        let primeraBolaLlegoAlFinal = true;
        let velocidadBolas = 1;
        let min = 0;
        let max = canciones.length-1;
       

        function reanudarCancion() {
        if (cancionActual !== null) {
            cancionActual.play();
        }
    }



        function crearCirculo(color) {
            var nuevoCirculo = document.createElement("div");
            nuevoCirculo.setAttribute("class", "circulo");
            nuevoCirculo.style.backgroundColor = color;
            nuevoCirculo.dataset.velocidad = Math.random() * velocidadBolas + 1; // Ajusta la velocidad según la variable velocidadBolas
            contenedor.appendChild(nuevoCirculo);
            return nuevoCirculo;
        }

        function crearCuadro(color, letra) {
            var nuevoCuadro = document.createElement("div");
            nuevoCuadro.setAttribute("class", "cuadro");
            nuevoCuadro.style.backgroundColor = color;
            nuevoCuadro.dataset.letra = letra;
            contenedor.appendChild(nuevoCuadro);
            return nuevoCuadro;
        }

        var colores = ["red", "green", "blue", "yellow", "orange"];
        var letras = ["d", "f", "g", "h", "j"];

        var circulos = [];
        var cuadros = [];

        function generarBolas() {
            if (primeraBolaLlegoAlFinal) {
                for (var i = 0; i < 5; i++) {
                    var nuevoCirculo = crearCirculo(colores[i]);
                    circulos.push(nuevoCirculo);
                    nuevoCirculo.style.top = i * 85 + "px";
                    nuevoCirculo.style.left = "0px";

                    var nuevoCuadro = crearCuadro(colores[i], letras[i]);
                    cuadros.push(nuevoCuadro);
                    nuevoCuadro.style.top = i * 85 + "px";
                    nuevoCuadro.style.left = contenedor.offsetWidth - 40 + "px";
                }
                primeraBolaLlegoAlFinal = false;
            }
        }

        generarBolas();

        document.addEventListener("keydown", function (evento) {
    var letraPresionada = evento.key.toLowerCase();
    cuadros.forEach(function (cuadro, index) {
        if (cuadro.dataset.letra === letraPresionada) {
            var anchoActual = parseFloat(cuadro.style.width) || 30;
            var nuevoAncho = anchoActual * 1.15;

            // Verificar si el nuevo ancho supera un límite
            var anchoMaximo = 40; // Establece el ancho máximo deseado
            if (nuevoAncho <= anchoMaximo) {
                cuadro.style.width = nuevoAncho + "px";
                cuadro.style.height = nuevoAncho + "px";
            }

            var cuadroRect = cuadro.getBoundingClientRect();
            circulos.forEach(function (circulo, circuloIndex) {
                var circuloRect = circulo.getBoundingClientRect();
                if (
                    cuadroRect.left < circuloRect.right &&
                    cuadroRect.right > circuloRect.left &&
                    cuadroRect.top < circuloRect.bottom &&
                    cuadroRect.bottom > circuloRect.top &&
                    cuadro.style.backgroundColor === circulo.style.backgroundColor
                ) {
                    puntos++;
                    tableroPuntuacion.textContent = puntos;
                    circulo.remove();
                    circulos.splice(circuloIndex, 1);
                    primeraBolaLlegoAlFinal = true;
                    generarBola();
                }
            });
        }
    });
});

        document.getElementById("reanudar").addEventListener("click", function () {
        isPaused = false;
        reanudarCancion();
    });

    

         document.getElementById("iniciar_juego").addEventListener("click", function () {
            
            if (!modoSeleccionado()) {
            alert("Debes seleccionar un modo de juego antes de iniciar.");
            return;

           
        }
            isPaused = false;
            reiniciarBolas();
            let x = Math.floor(Math.random()*(max-min+1)+min);
            if(cancionActual!=null){
                cancionActual.pause()
            }
            cancionActual=canciones[x]
            cancionActual.play()
            


        }); 


        document.getElementById("pausa").addEventListener("click", function () {
            isPaused = true;
            cancionActual.pause()
            
        });

        document.addEventListener("keyup", function (evento) {
            var letraSoltada = evento.key.toLowerCase();
            cuadros.forEach(function (cuadro) {
                if (cuadro.dataset.letra === letraSoltada) {
                    cuadro.style.width = "30px";
                    cuadro.style.height = "30px";
                }
            });
        });

        // Funciones para ajustar la velocidad según el modo seleccionado
        document.getElementById("facil").addEventListener("click", function () {
            reiniciarPuntuacion();
            cancionActual.pause()
            isPaused = true
            velocidadBolas = 1;
            reiniciarBolas();
        

        });

        document.getElementById("normal").addEventListener("click", function () {
            reiniciarPuntuacion();
            cancionActual.pause()
            isPaused = true
            velocidadBolas = 2;
            reiniciarBolas();
           

        });

        document.getElementById("dificil").addEventListener("click", function () {
            reiniciarPuntuacion();
            cancionActual.pause()
            isPaused = true
            velocidadBolas = 4;
            reiniciarBolas();

        });



        function modoSeleccionado() {
        return document.getElementById("facil").checked ||
            document.getElementById("normal").checked ||
            document.getElementById("dificil").checked;
    }


        function reiniciarPuntuacion() {
        puntos = 0;
        tableroPuntuacion.textContent = puntos;
    }


        function reiniciarBolas() {
            circulos.forEach(function (circulo) {
                circulo.remove();
            });
            cuadros.forEach(function (cuadro, index) {
                var nuevoCirculo = crearCirculo(colores[index]);
                circulos.push(nuevoCirculo);
                nuevoCirculo.style.top = index * 85 + "px";
                nuevoCirculo.style.left = "0px";
                cuadro.style.display = "block";
            });
            primeraBolaLlegoAlFinal = true;
        }

        var indexPelotaActual = 0; // Variable para mantener el índice de la pelota actual

        function generarBola() {
            var nuevoCirculo = crearCirculo(colores[indexPelotaActual]);
            circulos.push(nuevoCirculo);
            nuevoCirculo.style.top = indexPelotaActual * 85 + "px";
            nuevoCirculo.style.left = "0px";
            indexPelotaActual++; // Incrementar el índice para la próxima pelota
            if (indexPelotaActual >= 5) {
                indexPelotaActual = 0; // Reiniciar el índice si llegamos al final del arreglo de colores
            }
        }

        var intervalo = setInterval(function () {
            if (!isPaused) {
                if (circulos.length === 0 || parseFloat(circulos[circulos.length - 1].style.left) >= contenedor.offsetWidth - circulos[circulos.length - 1].offsetWidth) {
                    generarBola(); // Generar una nueva pelota si no hay ninguna o si la última ha llegado al final de la pantalla
                }
                circulos.forEach(function (circulo, index) {
                    var velocidad = parseFloat(circulo.dataset.velocidad);
                    var nuevaPosX = parseFloat(circulo.style.left) || 0;
                    nuevaPosX += velocidad;
                    circulo.style.left = nuevaPosX + "px";

                    if (nuevaPosX >= contenedor.offsetWidth - circulo.offsetWidth) {
                        circulo.style.left = "0px";
                        cuadros[index].style.display = "block";
                        primeraBolaLlegoAlFinal = true;
                    }
                });
            }
        }, 10);
    